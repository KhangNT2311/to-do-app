import App from './App'
import ReactDOM from 'react-dom'
import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { QueryParamProvider } from 'use-query-params'
import configureStore from 'store'
import { Provider } from 'react-redux'
import { createBrowserHistory } from 'history'
import { ConnectedRouter } from 'connected-react-router'
import { PersistGate } from 'redux-persist/integration/react'

const baseUrl =
  document.getElementsByTagName('base')[0].getAttribute('href') || '/'
const history = createBrowserHistory({ basename: baseUrl })

const { store, persistor } = configureStore(history)
const Root = (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <React.StrictMode>
        <ConnectedRouter history={history}>
          <QueryParamProvider ReactRouterRoute={Route}>
            <App />
          </QueryParamProvider>
        </ConnectedRouter>
      </React.StrictMode>
    </PersistGate>
  </Provider>
)
const rootElement = document.getElementById('app')
ReactDOM.render(Root, rootElement)
export { store }
