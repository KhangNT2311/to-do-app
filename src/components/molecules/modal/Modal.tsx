import React from 'react'
import MuiModal from '@material-ui/core/Modal'
import './Modal.scss'
import { useSelector } from 'react-redux'
import {
  isOpenSelector,
  modalPropsSelector,
  modalTypeSelector,
} from 'store/selectors/modal/modalSelector'
import ModalConfirm from './confirm/ModalConfirm'
import ModalMessage from './message/ModalMessage'
import ModalCustom from './custom/ModalCustom'
const MODAL_COMPONENTS = {
  CONFIRM: ModalConfirm,
  MESSAGE: ModalMessage,
  CUSTOM: ModalCustom,
}
const Modal: React.FC = () => {
  const isOpenModal = useSelector(isOpenSelector)
  const modalType = useSelector(modalTypeSelector)
  const modalProps = useSelector(modalPropsSelector)
  if (!modalType) {
    return null
  }
  const SpecificModal = MODAL_COMPONENTS[modalType]

  return <SpecificModal modalProps={modalProps} isOpen={isOpenModal} />
}
export default Modal
