import React from 'react'
import './ModalConfirm.scss'
import MuiModal from '@material-ui/core/Modal'
import Fade from 'components/atoms/fade/Fade'
import Backdrop from '@material-ui/core/Backdrop'
import { IModalConfirm, IModalConfirmProps } from './ModalConfirm.d'
import { useDispatch } from 'react-redux'
import { hideModal } from 'store/actions/modal/modalAction'
import Button from 'components/atoms/button/Button'
import { Container, makeStyles } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { AiOutlineCloseCircle } from 'react-icons/ai'

const useStyles = makeStyles({
  modal: {
    outline: 0,
  },
  button: {
    backgroundColor: '#E21936',
    color: '#fff',
    borderRadius: 8,
    padding: '16px 0',
    fontSize: '0.75rem',
    '&:hover': {
      backgroundColor: '#e21919ab',
    },
  },
  btnCancel: {
    backgroundColor: '#000',
    color: '#fff',
    borderRadius: 8,
    padding: '16px 0',
    fontSize: '0.75rem',
    '&:hover': {
      backgroundColor: '#000000bf',
    },
  },
  btnAgree: {
    backgroundColor: '#E21936',
    color: '#fff',
    borderRadius: 8,
    padding: '16px 0',
    fontSize: '0.75rem',
    '&:hover': {
      backgroundColor: '#e21919ab',
    },
  },
})
const ModalConfirm: React.FC<IModalConfirm> = ({ modalProps, isOpen }) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const { handleClose, handleConfirm, title, content } = modalProps
  const dispatch = useDispatch()
  const handleCloseModal = () => {
    handleClose?.()
    dispatch(hideModal())
  }
  const handleConfirmModal = () => {
    handleConfirm?.()
    dispatch(hideModal())
  }
  return (
    <div>
      <MuiModal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={`modal modal-confirm sm`}
        open={isOpen}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Container className={classes.modal}>
          <Fade in={isOpen}>
            <div className={`modal-content`}>
              <div
                className="modal-content__close"
                onClick={handleCloseModal}
              >
                <AiOutlineCloseCircle />
              </div>
              <div className="modal-content__wrapper">
                <div className="modal-content__title">{title}</div>
                <div className="modal-content__content">{content}</div>
                <div className="modal-content__footer">
                  <Button
                    onClick={handleCloseModal}
                    className={classes.btnCancel}
                    fullWidth={true}
                  >
                    {t('CANCEL')}
                  </Button>
                  <Button
                    className={classes.btnAgree}
                    fullWidth={true}
                    variant="contained"
                    onClick={handleConfirmModal}
                  >
                    {t('I_AGREE')}
                  </Button>
                </div>
              </div>
            </div>
          </Fade>
        </Container>
      </MuiModal>
    </div>
  )
}
export default ModalConfirm
