import React from "react";

export interface IModalConfirm {
    isOpen: boolean
    modalProps: IModalConfirmProps
}
export interface IModalConfirmProps {
    handleClose: () => void
    handleConfirm: () => void
    title?: React.ReactNode | string
    content: React.ReactNode | string
}