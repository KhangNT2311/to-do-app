export interface IModal {
  isOpen: boolean
  children: React.ReactNode
  handleClose: () => void
  size?: 'lg' | 'md' | 'sm'
  className?: string
}
