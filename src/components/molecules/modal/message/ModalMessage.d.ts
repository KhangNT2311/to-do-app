import React from "react";

export interface IModalMessage {
    isOpen: boolean
    modalProps: IModalMessageProps
}
export interface IModalMessageProps {
    handleConfirm: () => void
    title?: React.ReactNode | string
    content: React.ReactNode | string
}