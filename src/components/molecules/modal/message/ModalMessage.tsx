import React from 'react'
import './ModalMessage.scss'
import MuiModal from '@material-ui/core/Modal'
import Fade from 'components/atoms/fade/Fade'
import Backdrop from '@material-ui/core/Backdrop'
import { IModalMessage, IModalMessageProps } from './ModalMessage.d'
import { useDispatch } from 'react-redux'
import { hideModal } from 'store/actions/modal/modalAction'
import Button from 'components/atoms/button/Button'
import { Container, makeStyles } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
const useStyles = makeStyles({
  modal: {
    outline: 0,
  },
  button: {
    backgroundColor: '#E21936',
    color: '#fff',
    borderRadius: 8,
    padding: '16px 0',
    fontSize: '1.25rem',
    '&:hover': {
      backgroundColor: '#e21919ab',
    },
  },
  btnCancel: {
    backgroundColor: '#000',
    color: '#fff',
    borderRadius: 8,
    padding: '16px 0',
    fontSize: '1.25rem',
    '&:hover': {
      backgroundColor: '#000000bf',
    },
  },
  btnAgree: {
    backgroundColor: '#E21936',
    color: '#fff',
    borderRadius: 8,
    padding: '12px 0',
    fontSize: '1.25rem',
    '&:hover': {
      backgroundColor: '#e21919ab',
    },
  },
})
const ModalMessage: React.FC<IModalMessage> = ({ modalProps, isOpen }) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const { handleConfirm, title, content } = modalProps
  const dispatch = useDispatch()

  const handleConfirmModal = () => {
    handleConfirm?.()
    dispatch(hideModal())
  }
  return (
    <div>
      <MuiModal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={`modal modal-message sm`}
        open={isOpen}
        onClose={handleConfirmModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Container className={classes.modal}>
          <Fade in={isOpen}>
            <div className={`modal-content`}>
              <div className="modal-content__wrapper">
                <div className="modal-content__title">{title}</div>
                {content && (
                  <div
                    className="modal-content__content"
                    dangerouslySetInnerHTML={{
                      __html: content,
                    }}
                  ></div>
                )}
                <div className="modal-content__footer">
                  <Button
                    className={classes.btnAgree}
                    fullWidth={true}
                    variant="contained"
                    onClick={handleConfirmModal}
                  >
                    {t('OK')}
                  </Button>
                </div>
              </div>
            </div>
          </Fade>
        </Container>
      </MuiModal>
    </div>
  )
}
export default ModalMessage
