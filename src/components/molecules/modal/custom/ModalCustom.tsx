import React from 'react'
import './ModalCustom.scss'
import MuiModal from '@material-ui/core/Modal'
import Fade from 'components/atoms/fade/Fade'
import Backdrop from '@material-ui/core/Backdrop'
import { IModalCustomProps, IModalCustom } from './ModalCustom.d'
import { useDispatch } from 'react-redux'
import { hideModal } from 'store/actions/modal/modalAction'
import { Container, makeStyles } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { AiOutlineCloseCircle } from 'react-icons/ai'
const useStyles = makeStyles({
  modal: {
    outline: 0,
  },
  button: {
    backgroundColor: '#E21936',
    color: '#fff',
    borderRadius: 8,
    padding: '16px 0',
    fontSize: '1.25rem',
    '&:hover': {
      backgroundColor: '#e21919ab',
    },
  },
  btnCancel: {
    backgroundColor: '#000',
    color: '#fff',
    borderRadius: 8,
    padding: '16px 0',
    fontSize: '1.25rem',
    '&:hover': {
      backgroundColor: '#000000bf',
    },
  },
  btnAgree: {
    backgroundColor: '#E21936',
    color: '#fff',
    borderRadius: 8,
    padding: '12px 0',
    fontSize: '1.25rem',
    '&:hover': {
      backgroundColor: '#e21919ab',
    },
  },
})
const ModalCustom: React.FC<IModalCustom> = ({ modalProps, isOpen }) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const { content } = modalProps
  const dispatch = useDispatch()

  const handleConfirmModal = () => {
    dispatch(hideModal())
  }
  return (
    <div>
      <MuiModal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={`modal modal-message lg`}
        open={isOpen}
        onClose={handleConfirmModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Container className={classes.modal}>
          <Fade in={isOpen}>
            <div className={`modal-content`}>
              <div
                className="modal-content__close"
                onClick={handleConfirmModal}
              >
                <AiOutlineCloseCircle />
              </div>
              <div className="modal-content__wrapper">{content}</div>
            </div>
          </Fade>
        </Container>
      </MuiModal>
    </div>
  )
}
export default ModalCustom
