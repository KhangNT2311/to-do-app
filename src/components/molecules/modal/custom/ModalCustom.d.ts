import React from 'react'

export interface IModalCustom {
  isOpen: boolean
  modalProps: IModalCustomProps
}
export interface IModalCustomProps {
  content: React.ReactNode | string
}
