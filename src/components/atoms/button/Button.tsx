import React from 'react'
import { Button as ButtonMI, makeStyles } from '@material-ui/core'
import { IButton } from './Button.d'
const useStyles = makeStyles({
  button: {
    backgroundColor: '#E21936',
    color: '#fff',
    borderRadius: 8,
    padding: '12px 16px',
    fontSize: '0.875rem',
    '&:hover': {
      backgroundColor: '#000',
    },
  },
})
const Button: React.FC<IButton> = ({ className, children, ...props }) => {
  const classes = useStyles()

  return (
    <ButtonMI className={className || classes.button} {...props}>
      {children}
    </ButtonMI>
  )
}
export default Button
