export interface IFade {
  children: React.ReactNode
  in: boolean
  onEnter?: () => {}
  onExited?: () => {}
}
