import React, { ChangeEvent, KeyboardEventHandler } from 'react'
import { IInputField } from './InputField.d'
import { makeStyles, TextField } from '@material-ui/core'
import { FIELD_TYPE } from 'constants/forms'
import './InputField.scss'
const useStyles = makeStyles({
  root: {
    '& > div': {
      border: '1px solid #D9D9D9',
      borderRadius: 8,
      padding: '26px 15px 12px',
      marginTop: 0,
      display: "flex",
      alignItems: "flex-end",
      '& input': {
        padding: 0,
        height: '16px !important',
        fontSize: '1rem',

      },
      '& > div': {
        padding: 0,
        border: 'none',
      },
      '& p': {
        fontFamily: 'Montserrat',
        fontSize: '1rem',
      },
    },
    '& div:before, & div:after': {
      borderBottom: 'none !important',
    },
  },
  errorText: {
    fontFamily: 'Montserrat',
    fontSize: '1rem',
  },
  textField: {
    marginTop: '10px',
    paddingBottom: '6px',
  },
  label: {
    padding: '0 16px ',
    transform: 'translate(0px, 22px) scale(1)',
  },
  labelFocused: {
    transform: 'translate(3px, 10px) scale(0.8)',
  },
  labelAsterisk: {
    color: 'red',
    marginLeft: '-2px'
  },
  formControl: {
    backgroundColor: 'red !important',
  },

})

const InputField: React.FC<IInputField> = ({
  variant = 'standard',
  type,
  maxLength,
  ...props
}) => {
  const classes = useStyles()
  const handleKeyPress = (e: any) => {
    if (type === FIELD_TYPE.TEL) {
      e.target.value = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');

    }
  }
  return (
    <TextField
      {...props}
      variant={variant}
      className={classes.root}
      type={type}
      inputProps={{
        maxLength
      }}
      FormHelperTextProps={{
        className: classes.errorText,
      }}
      onKeyUp={handleKeyPress}
      InputLabelProps={{
        classes: {
          root: classes.label,
          focused: classes.labelFocused,
          shrink: classes.labelFocused,
          asterisk: classes.labelAsterisk,

        },
      }}
    />
  )
}
export default InputField
