import React from 'react'
import { ISelect } from './Select.d'
import { FormControl, FormHelperText, InputLabel, Select as MuiSelect } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import './Select.scss'
const useStyles = makeStyles({
  root: {
    marginTop: '0 !important',
    '& .MuiSelect-select:focus': {
      borderRadius: 8,
      backgroundColor: 'transparent',
    },
    '& > div': {
      border: '1px solid #D9D9D9',
      borderRadius: 8,
      padding: '28px 15px 11px',
      marginTop: 0,
      fontFamily: 'montserrat',
      '& input': {
        padding: 0,
      },
      '& > div': {
        padding: 0,
        border: 'none',
      },
      '& p': {
        fontFamily: 'Montserrat',
        fontSize: '1rem',
      },
    },
  },
  formControl: {
    width: '100%',
    '& div:before, & div:after': {
      borderBottom: 'none !important',
    },
  },
  errorText: {
    fontFamily: 'Montserrat',
    fontSize: '1rem',
    color: "#f44336"
  },
  textField: {
    marginTop: '10px',
    paddingBottom: '6px',
  },
  label: {
    padding: '0 16px ',
    transform: 'translate(0px, 15px) scale(1)',
    color: '#9F9F9F',
    fontFamily: 'Montserrat',
    fontSize: '0.75rem',
    fontWeight: 600,
  },
  labelFocused: {
    color: '#9F9F9F !important'
  },
  labelAsterisk: {
    color: 'red',
  },
})

const Select: React.FC<ISelect> = ({ required, helperText, hasError, children, label, ...props }) => {
  const classes = useStyles()
  return (
    <FormControl className={classes.formControl}>
      {label && <InputLabel className={classes.label} classes={{ focused: classes.labelFocused }}>{required ? <span>{label}<strong className="asterisk">*</strong></span> : label}</InputLabel>}
      <MuiSelect MenuProps={{
        disableScrollLock: true
      }} className={classes.root} {...props}>
        {children}
      </MuiSelect>
      {hasError && <FormHelperText className={classes.errorText}>{helperText}</FormHelperText>}
    </FormControl>
  )
}
export default Select
