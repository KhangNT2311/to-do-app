import { SelectProps } from '@material-ui/core'

export interface ISelect extends SelectProps {
  label?: string,
  hasError?: boolean,
  helperText?: string,
  required?: boolean
}
