import useDispatchMiddleware from './useDispatchMiddleware'

export default useDispatchMiddleware
