export interface IDispatchMiddlewareValues {
  types: string[]
  payload: any
  callApi: () => Promise<void>
  callbackSuccess?: Function
}
