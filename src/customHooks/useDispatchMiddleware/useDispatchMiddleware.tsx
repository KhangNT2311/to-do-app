import { useDispatch } from 'react-redux'
import { IDispatchMiddlewareValues } from './useDispatchMiddleware.d'
const useReducerWithMiddleware = () => {
  const rdxDispatch = useDispatch()
  const dispatchWithMiddleware = ({
    types,
    payload,
    callApi,
    callbackSuccess = null,
  }: IDispatchMiddlewareValues) => {
    const [request, success, fail] = types

    rdxDispatch({
      type: request,
    })
    if (callApi) {
      return callApi()
        .then((data: any) => {
          rdxDispatch({
            type: success,
            payload: { ...payload, data },
          })
          callbackSuccess?.()
        })
        .catch((error) => {
          rdxDispatch({
            type: fail,
            payload: {
              error,
            },
          })
        })
    }
    rdxDispatch({
      type: success,
      payload,
    })
  }
  const dispatch = (action: Function) => {
    action(dispatchWithMiddleware)
  }
  return [dispatch]
}
export default useReducerWithMiddleware
