import React, { Fragment, Suspense } from 'react'
import { IRoutes } from './Routes.d'
import { Switch, Route } from 'react-router'
import { routesConfig } from './routesConfig'
const renderRoutes: Function = (
  routes: IRoutes[],
  layout: React.FC | null = null,
  guard: React.FC | null = null
) => {
  return (
    routes &&
    routes.map((route: IRoutes, idx: number) => {
      const Layout = layout || Fragment
      const Guard = guard || Fragment
      const Component = route?.page || Fragment
      return route.routes && route.routes.length > 0
        ? renderRoutes(route.routes, route.layout, route.guard)
        : route?.path && (
            <Route
              key={`${route.path}-${idx}`}
              path={route?.path}
              exact={route?.exact}
            >
              <Guard>
                <Layout>
                  <Component />
                </Layout>
              </Guard>
            </Route>
          )
    })
  )
}

function Routes() {
  return (
    <Route
      render={({ location }) => (
        <Suspense fallback={<div />}>
          <Switch>{renderRoutes(routesConfig)}</Switch>
        </Suspense>
      )}
    />
  )
}

export default Routes
