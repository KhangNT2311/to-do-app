import { lazy } from 'react'
import { IRoutes } from './Routes.d'
import { RoutesString } from './routesString'

const HomePage = lazy(() => import('pages/home/HomePage'))
const TaskStatusPage = lazy(() => import('pages/taskStatus/TaskStatusPage'))

const Error404Page = lazy(() => import('pages/errors/error404/Error404'))

export const routesConfig: IRoutes[] = [
  {
    exact: true,
    page: HomePage,
    path: RoutesString.Home,
  },
  {
    exact: true,
    page: TaskStatusPage,
    path: RoutesString.TaskStatus,
  },
  {
    page: Error404Page,
    path: '*',
  },
]
