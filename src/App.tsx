import React from 'react'
import './i18n/i18n'
import Routes from 'routes'
import LinearContainer from 'containers/linearContainer/LinearContainer'
const App: React.FC = () => {
  return (
    <LinearContainer>
      <Routes />
      {/* <h1>Test </h1> */}
    </LinearContainer>
  )
}

export default App
