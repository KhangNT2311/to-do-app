import useDispatchMiddleware from 'customHooks/useDispatchMiddleware'
import HomeModule from 'modules/home/HomeModule'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { getTasks, getTaskStatuses } from 'store/actions/task/taskAction'
import { taskSelector } from 'store/selectors/task/taskSelector'
const HomePage: React.FC = () => {
  const [dispatch] = useDispatchMiddleware()
  const taskState = useSelector(taskSelector)
  const { isLoading, taskStatuses, rebound } = taskState
  useEffect(() => {
    if(rebound){
      dispatch(getTaskStatuses)

    }
  }, [rebound])
  const homeModuleProps = {
    isLoading,
    taskStatuses,
  }
  return <HomeModule {...homeModuleProps} />
}
export default HomePage
