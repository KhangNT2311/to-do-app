import useDispatchMiddleware from 'customHooks/useDispatchMiddleware'
import TaskStatusModule from 'modules/taskStatus/TaskStatusModule'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { getTaskStatuses } from 'store/actions/task/taskAction'
import { taskSelector } from 'store/selectors/task/taskSelector'

const TaskStatusPage: React.FC = () => {
  const [dispatch] = useDispatchMiddleware()
  const taskState = useSelector(taskSelector)
  const { isLoading, taskStatuses, rebound } = taskState
  useEffect(() => {
    if (rebound) {
      dispatch(getTaskStatuses)
    }
    return () => {
      dispatch(getTaskStatuses)
    }
  }, [rebound])
  const taskStatusModuleProps = {
    isLoading,
    taskStatuses,
  }
  return <TaskStatusModule {...taskStatusModuleProps} />
}

export default TaskStatusPage
