import { IConfig } from './api.d'

const REACT_APP_API_URL = 'http://localhost:3000/api/v1'

const config: IConfig = {
  API: {
    TASK_STATUS_SERVICES: '/taskStatuses',
    TASK_SERVICES: '/tasks',
  },
}

Object.keys(config.API).forEach(
  (item) => (config.API[item] = REACT_APP_API_URL + config.API[item])
)

export default config
