import axios from 'axios'
import { store } from '../index'
import { ILoadingType } from 'store/actions/loading/loadingAction.d'

// Add a request interceptor
axios.interceptors.request.use(
  function (config) {
    store.dispatch({
      type: ILoadingType.START_LOADING,
    })
    // Do something before request is sent
    return config
  },
  function (error) {
    store.dispatch({
      type: ILoadingType.START_LOADING,
    })
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
axios.interceptors.response.use(
  function (response) {
    store.dispatch({
      type: ILoadingType.FINISH_LOADING,
    })
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response
  },
  function (error) {
    store.dispatch({
      type: ILoadingType.FINISH_LOADING,
    })
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error)
  }
)
