import { ITaskStatusValues } from '../../store/reducers/task/taskReducer.d'

export interface IHomeModule {
  isLoading: boolean
  taskStatuses: ITaskStatusValues[]
}
