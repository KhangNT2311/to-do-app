import { useFormik } from 'formik'
import React, { useRef, useMemo } from 'react'
import JoditEditor from 'jodit-react'

import './TaskForm.scss'
import { CREATE_TASK_FIELD, FIELD_TYPE } from 'constants/forms'
import { ITaskForm } from './TaskForm.d'
import { IFieldValues } from 'constants/types'
import { get, update } from 'lodash'
import Select from 'components/atoms/select/Select'
import { MenuItem } from '@material-ui/core'
import InputField from 'components/atoms/inputField/InputField'
import Button from 'components/atoms/button/Button'
import { checkValueError, validateRequired } from 'utils/validation'
const validateFields = {
  taskName: [
    {
      validator: validateRequired,
      code: 'FORM_ERROR/REQUIRED_FIELD',
    },
  ],
  taskStatus: [
    {
      validator: validateRequired,
      code: 'FORM_ERROR/REQUIRED_FIELD',
    },
  ],
}
const TaskForm: React.FC<ITaskForm> = ({
  initialValues,
  taskStatuses,
  handleSubmit,
  isUpdate = false,
}) => {
  const editor = useRef(null)
  const formik = useFormik({
    initialValues,
    onSubmit: (values) => {
      const { _id, ...updatedValues } = values
      handleSubmit(isUpdate ? values : updatedValues)
    },
    validate: checkValueError(validateFields),
  })
  const {
    errors,
    touched,
    values,
    handleChange,
    handleSubmit: onSubmit,
    handleBlur,
    setFieldValue,
  } = formik

  const config = {
    readonly: false,
    height: '280px',
    uploader: {
      insertImageAsBase64URI: true,
    },
  }
  const { TASK_DESCRIPTION, TASK_NAME, TASK_STATUS } = CREATE_TASK_FIELD
  const fields = [
    {
      id: `${TASK_NAME}Id`,
      name: TASK_NAME,
      label: 'TASK NAME',
      required: true,
      type: FIELD_TYPE.TEXT,
    },
    {
      id: `${TASK_DESCRIPTION}Id`,
      name: TASK_DESCRIPTION,
      label: 'TASK DESCRIPTION',
      required: true,
      type: FIELD_TYPE.EDITOR,
    },
    {
      id: `${TASK_STATUS}Id`,
      name: TASK_STATUS,
      label: 'TASK STATUS',
      required: true,
      type: FIELD_TYPE.SELECT,
      options: taskStatuses.map((taskStatus) => ({
        value: taskStatus._id,
        label: taskStatus.taskStatusName,
      })),
    },
  ]
  const handleChangeJodit = (name: string) => (value: string) => {
    setFieldValue(name, value)
  }
  const renderFields = (fields: IFieldValues[]) => {
    return fields.map((field, index) => {
      const value = field.value || get(values, field.name, '')
      const error =
        touched?.[field.name] && Boolean(errors?.[field.name] || false)
      const helperText = (touched?.[field.name] && errors?.[field.name]) || ''

      return (
        <div className="form__field" key={`${field.id}-${index}`}>
          {field.type === FIELD_TYPE.SELECT ? (
            <Select
              {...field}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error}
              value={value}
              defaultValue={value}
              label={field?.label?.toUpperCase()}
            >
              {field.options &&
                field.options.map(
                  (option: { value: string; label: string }, idx: number) => (
                    <MenuItem key={`menu-item-${idx}`} value={option.value}>
                      {option.label}
                    </MenuItem>
                  )
                )}
            </Select>
          ) : field.type === FIELD_TYPE.EDITOR ? (
            useMemo(() => {
              return (
                <JoditEditor
                  ref={editor}
                  value={value || ''}
                  config={config}
                  onBlur={handleChangeJodit(CREATE_TASK_FIELD.TASK_DESCRIPTION)}
                />
              )
            }, [values.taskDescription])
          ) : (
            <InputField
              fullWidth
              {...field}
              value={value}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error}
              helperText={helperText}
            />
          )}
          {field?.endAdornment || ''}
        </div>
      )
    })
  }
  return (
    <div className="createTaskForm">
      <div className="createTaskForm__wrapper">
        <h1>{isUpdate ? 'Update Task' : 'Create Task'}</h1>
        <form className="form" onSubmit={onSubmit}>
          {renderFields(fields)}
          <div className="form__submit">
            <Button type="submit">{isUpdate ? 'Update' : 'Create'}</Button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default TaskForm
