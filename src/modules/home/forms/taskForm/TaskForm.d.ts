import { ITaskStatusValues } from 'store/reducers/task/taskReducer.d'

export interface ITaskForm {
  handleSubmit: (values: ICreateTaskFormValues) => void
  taskStatuses: ITaskStatusValues[]
  initialValues: ICreateTaskFormValues
  isUpdate?: boolean
}

export interface ICreateTaskFormValues {
  taskName: string
  taskStatus: string
  taskDescription?: string
  _id?: string
}
