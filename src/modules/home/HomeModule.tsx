import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import _ from 'lodash'
import './HomeModule.scss'
import { useDispatch } from 'react-redux'
import { hideModal, openModal } from 'store/actions/modal/modalAction'
import { MODAL_TYPE } from 'constants/enums'
import { IHomeModule } from './HomeModule.d'
import { ITaskStatusValues } from 'store/reducers/task/taskReducer.d'
import { ICreateTaskFormValues } from './forms/taskForm/TaskForm.d'
import {
  createTask,
  changeTaskStatus,
  updateTask,
} from 'store/actions/task/taskAction'
import useDispatchMiddleware from 'customHooks/useDispatchMiddleware'
import { CircularProgress } from '@material-ui/core'
import TaskForm from './forms/taskForm/TaskForm'
import Button from 'components/atoms/button/Button'
import { useHistory } from 'react-router-dom'
import { RoutesString } from 'routes/routesString'
const HomeModule: React.FC<IHomeModule> = ({ isLoading, taskStatuses }) => {
  const { push } = useHistory()
  const [dispatch] = useDispatchMiddleware()
  const [currentTaskStatuses, setCurrentTaskStatuses] = useState(taskStatuses)
  const normalDispatch = useDispatch()
  useEffect(() => {
    setCurrentTaskStatuses(taskStatuses)
  }, [taskStatuses])

  const handleDragEnd = ({ destination, source }: any) => {
    if (!destination) {
      return
    }

    if (
      destination.index === source.index &&
      destination.droppableId === source.droppableId
    ) {
      return
    }

    const updatedTaskStatuses = [...currentTaskStatuses]
    const itemSource = currentTaskStatuses.find(
      (item) => item._id === source.droppableId
    )
    const itemDes = currentTaskStatuses.find(
      (item) => item._id === destination.droppableId
    )
    const itemRemoved = itemSource?.tasks.splice(source.index, 1)
    if (itemRemoved?.[0]) {
      itemRemoved[0].taskStatus = itemDes?._id || ''
      itemDes?.tasks.splice(destination.index, 0, itemRemoved[0])
    }

    setCurrentTaskStatuses(updatedTaskStatuses)
    const updatedSource = {
      index: source.index,
      statusId: source.droppableId,
    }
    const updatedDestination = {
      index: destination.index,
      statusId: destination.droppableId,
    }
    dispatch(changeTaskStatus(updatedSource, updatedDestination))
  }
  const handleSubmitTask =
    (isUpdate: boolean = false) =>
    (values: ICreateTaskFormValues) => {
      const closeModal = () => {
        normalDispatch(hideModal())
      }
      if (isUpdate) {
        dispatch(updateTask(values, closeModal))
      } else {
        dispatch(createTask(values, closeModal))
      }
    }
  const handleClickTask =
    (task: ICreateTaskFormValues | null = null) =>
    () => {
      const initFields = {
        taskName: task?.taskName || '',
        taskStatus: task?.taskStatus || '',
        taskDescription: task?.taskDescription || '',
        _id: task?._id || '',
      }
      const modalProps = {
        content: (
          <TaskForm
            initialValues={initFields}
            taskStatuses={taskStatuses}
            handleSubmit={handleSubmitTask(!!task)}
            isUpdate={!!task}
          />
        ),
      }
      normalDispatch(openModal(MODAL_TYPE.CUSTOM, modalProps))
    }
  const renderDndItems = useCallback(
    (taskStatuses: ITaskStatusValues[]) => {
      return (
        taskStatuses &&
        taskStatuses.map(({ _id, taskStatusName, tasks }, key) => (
          <div key={key} className={'column'}>
            <h3>{taskStatusName}</h3>
            <div className="droppableWrapper">
              <Droppable droppableId={_id}>
                {(provided, snapshot) => {
                  return (
                    <div
                      ref={provided.innerRef}
                      {...provided.droppableProps}
                      className={`droppable-col ${
                        snapshot.isDraggingOver ? 'dragging' : ''
                      }`}
                    >
                      {tasks.map((task, index) => {
                        return (
                          <Draggable
                            key={task._id}
                            index={index}
                            draggableId={task._id}
                          >
                            {(provided, snapshot) => {
                              return (
                                <div
                                  onClick={handleClickTask(task)}
                                  className={`item ${
                                    snapshot.isDragging && 'dragging'
                                  }`}
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                >
                                  {task.taskName}
                                </div>
                              )
                            }}
                          </Draggable>
                        )
                      })}
                      {provided.placeholder}
                    </div>
                  )
                }}
              </Droppable>
            </div>
          </div>
        ))
      )
    },
    [currentTaskStatuses]
  )
  const handleClickTaskStatus = () => {
    push(RoutesString.TaskStatus)
  }
  return (
    <div className="home">
      <div className="home__wrapper">
        <div className="home__title">
          <h1>To Do App</h1>
        </div>
        <div className="home__taskStatus">
          <Button onClick={handleClickTaskStatus}>
            Task Status Management
          </Button>
        </div>
        <div className="home__create">
          <Button onClick={handleClickTask()}>Create Task</Button>
        </div>
        {isLoading ? (
          <div className="loadingWrapper">
            <CircularProgress />
          </div>
        ) : (
          <div
            className="home__content"
            style={{
              gridTemplateColumns: `repeat(${currentTaskStatuses.length},1fr)`,
            }}
          >
            <DragDropContext onDragEnd={handleDragEnd}>
              {renderDndItems(currentTaskStatuses)}
            </DragDropContext>
          </div>
        )}
      </div>
    </div>
  )
}

export default HomeModule
