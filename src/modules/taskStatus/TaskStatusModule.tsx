import { CircularProgress } from '@material-ui/core'
import React, { useCallback, useEffect, useState } from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { ITaskStatusValues } from 'store/reducers/task/taskReducer.d'
import { ITaskStatusModule } from './TaskStatusModule.d'
import { AiOutlineLeft } from 'react-icons/ai'
import { BsTrash } from 'react-icons/bs'
import './TaskStatusModule.scss'
import useDispatchMiddleware from 'customHooks/useDispatchMiddleware'
import Button from 'components/atoms/button/Button'
import {
  createTaskStatus,
  deleteTaskStatus,
  reorderTaskStatus,
} from 'store/actions/task/taskAction'
import { useDispatch } from 'react-redux'
import { hideModal, openModal } from 'store/actions/modal/modalAction'
import { MODAL_TYPE } from 'constants/enums'
import TaskStatusForm from './forms/TaskStatusForm'
import { ICreateTaskStatusFormValues } from './forms/TaskStatusForm.d'
import { RoutesString } from 'routes/routesString'
import { useHistory } from 'react-router-dom'
const TaskStatusModule: React.FC<ITaskStatusModule> = ({
  isLoading,
  taskStatuses,
}) => {
  const { push } = useHistory()
  const [currentTaskStatuses, setCurrentTaskStatuses] = useState<
    ITaskStatusValues[]
  >([])
  const [dispatch] = useDispatchMiddleware()
  const normalDispatch = useDispatch()
  useEffect(() => {
    setCurrentTaskStatuses(taskStatuses)
  }, [taskStatuses])
  const handleDragEnd = ({ destination, source }: any) => {
    if (!destination) {
      return
    }

    const updatedTaskStatuses = [...currentTaskStatuses]
    const [removed] = updatedTaskStatuses.splice(source.index, 1)
    updatedTaskStatuses.splice(destination.index, 0, removed)

    setCurrentTaskStatuses(updatedTaskStatuses)
    dispatch(reorderTaskStatus(updatedTaskStatuses))
  }
  const handleClickRemove = (id: string) => () => {
    const modalProps = {
      handleConfirm: () => {
        dispatch(deleteTaskStatus(id))
      },
      title: 'Notification',
      content:
        'Are you sure you want to delete?, it also delete all tasks in this status',
    }
    normalDispatch(openModal(MODAL_TYPE.CONFIRM, modalProps))
  }
  const handleSubmitTaskStatus =
    (isUpdate: boolean = false) =>
    (values: ICreateTaskStatusFormValues) => {
      const closeModal = () => {
        normalDispatch(hideModal())
      }
      if (isUpdate) {
        dispatch(createTaskStatus(values, closeModal))
      } else {
        dispatch(createTaskStatus(values, closeModal))
      }
    }
  const handleClickTaskStatus =
    (taskStatus: ICreateTaskStatusFormValues | null = null) =>
    () => {
      const initFields = {
        taskStatusName: taskStatus?.taskStatusName || '',
        _id: taskStatus?._id || '',
      }
      const modalProps = {
        content: (
          <TaskStatusForm
            initialValues={initFields}
            handleSubmit={handleSubmitTaskStatus(!!taskStatus)}
            isUpdate={!!taskStatus}
          />
        ),
      }
      normalDispatch(openModal(MODAL_TYPE.CUSTOM, modalProps))
    }
  const renderDndItems = useCallback(
    (taskStatuses: ITaskStatusValues[]) => {
      return (
        <Droppable droppableId={'droppableId'} direction="horizontal">
          {(provided, snapshot) => {
            return (
              <div
                ref={provided.innerRef}
                {...provided.droppableProps}
                style={{
                  gridTemplateColumns: `repeat(${taskStatuses.length},1fr)`,
                }}
                className={`droppable-row ${
                  snapshot.isDraggingOver ? 'dragging' : ''
                }`}
              >
                {taskStatuses &&
                  taskStatuses.map((taskStatus, index) => {
                    return (
                      <Draggable
                        key={taskStatus._id}
                        index={index}
                        draggableId={taskStatus._id}
                      >
                        {(provided, snapshot) => {
                          return (
                            <div
                              className={`item ${
                                snapshot.isDragging && 'dragging'
                              }`}
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                            >
                              <div
                                className="item__remove"
                                onClick={handleClickRemove(taskStatus._id)}
                              >
                                <BsTrash />
                              </div>

                              <div className="item__name">
                                {taskStatus.taskStatusName}
                              </div>
                            </div>
                          )
                        }}
                      </Draggable>
                    )
                  })}
                {provided.placeholder}
              </div>
            )
          }}
        </Droppable>
      )
    },
    [currentTaskStatuses]
  )
  const handleGoBack = () => {
    push(RoutesString.Home)
  }
  return (
    <div className="taskStatus">
      <div className="taskStatus__wrapper">
        <div className="taskStatus__title">
          <AiOutlineLeft onClick={handleGoBack} />
          <h1>Task Status Management</h1>
        </div>
        <div className="taskStatus__create">
          <Button onClick={handleClickTaskStatus()}>Create Task Status</Button>
        </div>
        {isLoading ? (
          <div className="loadingWrapper">
            <CircularProgress />
          </div>
        ) : (
          <div
            className="taskStatus__content"
            style={{
              gridTemplateColumns: `repeat(${currentTaskStatuses.length},1fr)`,
            }}
          >
            <DragDropContext onDragEnd={handleDragEnd}>
              {renderDndItems(currentTaskStatuses)}
            </DragDropContext>
          </div>
        )}
      </div>
    </div>
  )
}
export default TaskStatusModule
