import { ITaskStatusValues } from 'store/reducers/task/taskReducer.d'

export interface ITaskStatusForm {
  handleSubmit: (values: ICreateTaskStatusFormValues) => void
  initialValues: ICreateTaskStatusFormValues
  isUpdate?: boolean
}

export interface ICreateTaskStatusFormValues {
  taskStatusName: string
  _id?: string
}
