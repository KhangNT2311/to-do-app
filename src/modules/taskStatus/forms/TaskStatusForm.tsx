import Button from 'components/atoms/button/Button'
import InputField from 'components/atoms/inputField/InputField'
import { TASK_STATUS_FIELD } from 'constants/forms'
import { useFormik } from 'formik'
import React from 'react'
import { checkValueError, validateRequired } from 'utils/validation'
import { ITaskStatusForm } from './TaskStatusForm.d'
import './TaskStatusForm.scss'
const validateFields = {
  taskStatusName: [
    {
      validator: validateRequired,
      code: 'FORM_ERROR/REQUIRED_FIELD',
    },
  ],
}
const TaskStatusForm: React.FC<ITaskStatusForm> = ({
  handleSubmit,
  initialValues,
  isUpdate,
}) => {
  const formik = useFormik({
    initialValues,
    onSubmit: (values) => {
      const { _id, ...updatedValues } = values
      handleSubmit(isUpdate ? values : updatedValues)
    },
    validate: checkValueError(validateFields),
  })
  const {
    errors,
    touched,
    values,
    handleChange,
    handleSubmit: onSubmit,
    handleBlur,
  } = formik
  return (
    <form className="taskStatusForm" onSubmit={onSubmit}>
      <div className="taskStatusForm__wrapper">
        <h1>Add Status</h1>
        <InputField
          fullWidth
          name={TASK_STATUS_FIELD.TASK_STATUS_NAME}
          value={values.taskStatusName}
          onChange={handleChange}
          onBlur={handleBlur}
          label="Status Name"
          error={
            touched?.taskStatusName && Boolean(errors?.taskStatusName || false)
          }
          helperText={touched?.taskStatusName && errors?.taskStatusName}
        />
        <div className="taskStatusForm__btn">
          <Button type="submit">Create</Button>
        </div>
      </div>
    </form>
  )
}

export default TaskStatusForm
