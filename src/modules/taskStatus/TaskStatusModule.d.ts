import { ITaskStatusValues } from '../../store/reducers/task/taskReducer.d'

export interface ITaskStatusModule {
  isLoading: boolean
  taskStatuses: ITaskStatusValues[]
}
