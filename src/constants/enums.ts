export enum MODAL_TYPE {
  MESSAGE = 'MESSAGE',
  CONFIRM = 'CONFIRM',
  CUSTOM = 'CUSTOM',
}

export enum METHOD {
  GET = 'GET',
  POST = 'POST',
  DELETE = 'DELETE',
  PUT = 'PUT',
}
