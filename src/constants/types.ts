export type FnType = (param?: object) => void

export interface IFieldOptionValues {
  value: any
  label: string
}

export interface IFieldValues {
  id: string
  name: string
  label?: string
  type?: string
  options?: IFieldOptionValues[]
  [key: string]: any
}
