export function getProperty<T, K extends keyof T>(obj: T, key: K) {
  return obj[key] 
}
export function setProperty<T, K extends keyof T>(obj: T, key: K, value: T[K]) {
  obj[key] = value
}
export function throttle(callback: Function, limit: number) {
  var waiting = false 
  return function () {
    if (!waiting) {
      callback.apply(this, arguments)
      waiting = true
      setTimeout(function () {
        waiting = false 
      }, limit)
    }
  }
}

export function debounce(func: Function, wait: number, immediate = false) {
  let timeout: any

  return function () {
    const context = this,
      args = arguments
    var callNow = immediate && !timeout
    clearTimeout(timeout)

    timeout = setTimeout(function () {
      timeout = null

      if (!immediate) {
        func.apply(context, args)
      }
    }, wait)

    if (callNow) func.apply(context, args)
  }
}
