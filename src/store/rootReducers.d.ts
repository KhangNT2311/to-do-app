import { IAppState } from './reducers/app/appReducer.d'
import { IAuthState } from './reducers/auth/authReducer.d'
import { IModalState } from './reducers/modal/modalReducer.d'
import { ILoadingState } from './reducers/loading/loadingReducer.d'
import { ITaskState } from './reducers/task/taskReducer.d'
export interface IRootState {
  auth: IAuthState
  app: IAppState
  modal: IModalState
  loading: ILoadingState
  task: ITaskState
}
