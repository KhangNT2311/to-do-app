import { connectRouter } from 'connected-react-router'
import { combineReducers } from 'redux'
import auth from './reducers/auth/authReducer'
import app from './reducers/app/appReducer'
import modal from './reducers/modal/modalReducer'
import loading from './reducers/loading/loadingReducer'
import task from './reducers/task/taskReducer'
const createRootReducer: Function = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    auth,
    app,
    modal,
    loading,
    task,
  })
export default createRootReducer
