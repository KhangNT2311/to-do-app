import API from 'api'
import config from 'api/config'
import { METHOD } from 'constants/enums'
import { ICreateTaskFormValues } from 'modules/home/forms/taskForm/TaskForm.d'
import { ICreateTaskStatusFormValues } from 'modules/taskStatus/forms/TaskStatusForm.d'
import {
  ITaskStatusValues,
  ITaskValues,
} from 'store/reducers/task/taskReducer.d'
import { ITaskType } from './taskAction.d'
const {
  GET_TASK_STATUSES_FAILURE,
  GET_TASK_STATUSES_REQUEST,
  GET_TASK_STATUSES_SUCCESS,
  GET_TASKS_FAILURE,
  GET_TASKS_REQUEST,
  GET_TASKS_SUCCESS,
  CREATE_TASK_FAILURE,
  CREATE_TASK_REQUEST,
  CREATE_TASK_SUCCESS,
  UPDATE_TASK_FAILURE,
  UPDATE_TASK_REQUEST,
  UPDATE_TASK_SUCCESS,
  CHANGE_TASK_STATUS_FAILURE,
  CHANGE_TASK_STATUS_REQUEST,
  CHANGE_TASK_STATUS_SUCCESS,
  DELETE_TASK_STATUS_FAILURE,
  DELETE_TASK_STATUS_REQUEST,
  DELETE_TASK_STATUS_SUCCESS,
  CREATE_TASK_STATUS_FAILURE,
  CREATE_TASK_STATUS_REQUEST,
  CREATE_TASK_STATUS_SUCCESS,
  REORDER_TASK_STATUS_FAILURE,
  REORDER_TASK_STATUS_REQUEST,
  REORDER_TASK_STATUS_SUCCESS,
} = ITaskType
export const getTaskStatuses = (dispatch: Function) => {
  const dispatchProps = {
    types: [
      GET_TASK_STATUSES_REQUEST,
      GET_TASK_STATUSES_SUCCESS,
      GET_TASK_STATUSES_FAILURE,
    ],
    callApi: () =>
      API({
        url: config.API.TASK_STATUS_SERVICES,
        method: METHOD.GET,
      }),
  }
  dispatch(dispatchProps)
}

export const getTasks = (dispatch: Function) => {
  const dispatchProps = {
    types: [GET_TASKS_REQUEST, GET_TASKS_SUCCESS, GET_TASKS_FAILURE],
    callApi: () =>
      API({
        url: config.API.TASK_SERVICES,
        method: METHOD.GET,
      }),
  }
  dispatch(dispatchProps)
}

export const createTask =
  (data: ICreateTaskFormValues, callbackSuccess: Function) =>
  (dispatch: Function) => {
    const dispatchProps = {
      types: [CREATE_TASK_REQUEST, CREATE_TASK_SUCCESS, CREATE_TASK_FAILURE],
      callApi: () =>
        API({
          url: config.API.TASK_SERVICES,
          method: METHOD.POST,
          data,
        }),
      callbackSuccess,
    }
    dispatch(dispatchProps)
  }

export const changeTaskStatus =
  (
    source: { index: number; statusId: string },
    destination: { index: number; statusId: string }
  ) =>
  (dispatch: Function) => {
    const dispatchProps = {
      types: [
        CHANGE_TASK_STATUS_REQUEST,
        CHANGE_TASK_STATUS_SUCCESS,
        CHANGE_TASK_STATUS_FAILURE,
      ],
      callApi: () =>
        API({
          url: `${config.API.TASK_STATUS_SERVICES}/changeStatus`,
          method: METHOD.PUT,
          data: {
            source,
            destination,
          },
        }),
    }
    dispatch(dispatchProps)
  }

export const updateTask =
  (data: ICreateTaskFormValues, callbackSuccess: Function) =>
  (dispatch: Function) => {
    const { _id, ...updatedData } = data
    const dispatchProps = {
      types: [UPDATE_TASK_REQUEST, UPDATE_TASK_SUCCESS, UPDATE_TASK_FAILURE],
      callApi: () =>
        API({
          url: `${config.API.TASK_SERVICES}/${_id}`,
          method: METHOD.PUT,
          data: updatedData,
        }),
      callbackSuccess,
    }
    dispatch(dispatchProps)
  }

export const deleteTaskStatus = (id: string) => (dispatch: Function) => {
  const dispatchProps = {
    types: [
      DELETE_TASK_STATUS_REQUEST,
      DELETE_TASK_STATUS_SUCCESS,
      DELETE_TASK_STATUS_FAILURE,
    ],
    callApi: () =>
      API({
        url: `${config.API.TASK_STATUS_SERVICES}/${id}`,
        method: METHOD.DELETE,
      }),
  }
  dispatch(dispatchProps)
}

export const createTaskStatus =
  (data: ICreateTaskStatusFormValues, callbackSuccess: Function) =>
  (dispatch: Function) => {
    const dispatchProps = {
      types: [
        CREATE_TASK_STATUS_REQUEST,
        CREATE_TASK_STATUS_SUCCESS,
        CREATE_TASK_STATUS_FAILURE,
      ],
      callApi: () =>
        API({
          url: config.API.TASK_STATUS_SERVICES,
          method: METHOD.POST,
          data,
        }),
      callbackSuccess,
    }
    dispatch(dispatchProps)
  }

export const reorderTaskStatus =
  (updatedTaskStatuses: ITaskStatusValues[]) => (dispatch: Function) => {
    const dispatchProps = {
      types: [
        REORDER_TASK_STATUS_REQUEST,
        REORDER_TASK_STATUS_SUCCESS,
        REORDER_TASK_STATUS_FAILURE,
      ],
      callApi: () =>
        API({
          url: `${config.API.TASK_STATUS_SERVICES}/reorder`,
          method: METHOD.PUT,
          data: {
            updatedTaskStatuses,
          },
        }),
    }
    dispatch(dispatchProps)
  }
