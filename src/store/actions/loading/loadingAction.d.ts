export enum ILoadingType {
    START_LOADING = 'LOADING/START_LOADING',
    FINISH_LOADING = 'LOADING/FINISH_LOADING'
}