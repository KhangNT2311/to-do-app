import { Dispatch } from "react"
import { ILoadingType } from "./loadingAction.d"

export const startFetching = () => (dispatch: Dispatch<{}>) => {
    dispatch({ type: ILoadingType.START_LOADING })
}

export const finishFetching = () => (dispatch: Dispatch<{}>) => {
    dispatch({ type: ILoadingType.FINISH_LOADING })
}