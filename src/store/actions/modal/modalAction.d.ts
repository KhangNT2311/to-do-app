export enum IModalType {
    OPEN_MODAL = "MODAL/OPEN_MODAL",
    HIDE_MODAL = "MODAL/HIDE_MODAL"
}