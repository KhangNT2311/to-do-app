import { Dispatch } from 'react'
import { IModalType } from './modalAction.d'
export const openModal =
  (type: string, props: any) => (dispatch: Dispatch<any>) => {
    dispatch({
      type: IModalType.OPEN_MODAL,
      payload: { modalType: type, modalProps: { ...props } },
    })
  }

export const hideModal = () => (dispatch: Dispatch<any>) => {
  dispatch({ type: IModalType.HIDE_MODAL })
}
