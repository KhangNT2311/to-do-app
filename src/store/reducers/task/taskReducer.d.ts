export interface ITaskState {
  isLoading: boolean
  tasks: ITaskValues[]
  taskStatuses: ITaskStatusValues[]
  rebound: boolean
  error: string
}

export interface ITaskValues {
  taskName: string
  taskDescription?: string
  taskImages?: string[]
  sortPriority: number
  taskStatus: string | ITaskStatusValues
  _id: string
}
export interface ITaskStatusValues {
  taskStatusName: string
  sortPriority?: number
  tasks: ITaskValues[]
  _id: string
}
