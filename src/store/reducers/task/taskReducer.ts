import { ITaskType } from 'store/actions/task/taskAction.d'
import { IAppCreator } from '../app/appReducer.d'
import { ITaskState, ITaskValues, ITaskStatusValues } from './taskReducer.d'
import { toast } from 'react-toastify'

const {
  GET_TASK_STATUSES_REQUEST,
  GET_TASK_STATUSES_FAILURE,
  GET_TASK_STATUSES_SUCCESS,
  CREATE_TASK_SUCCESS,
  UPDATE_TASK_SUCCESS,
  DELETE_TASK_STATUS_SUCCESS,
  CREATE_TASK_STATUS_SUCCESS,
} = ITaskType
const initialState: ITaskState = {
  isLoading: false,
  taskStatuses: [],
  tasks: [],
  rebound: true,
  error: '',
}

export default (state = initialState, { type, payload }: IAppCreator) => {
  switch (type) {
    case GET_TASK_STATUSES_REQUEST:
      return { ...state, isLoading: true }
    case GET_TASK_STATUSES_SUCCESS:
      return {
        ...state,
        taskStatuses: payload.data,
        isLoading: false,
        rebound: false,
      }
    case GET_TASK_STATUSES_FAILURE:
      return { ...state, ...payload, isLoading: false }

    case CREATE_TASK_SUCCESS:
      toast.success('Create task successfully')
      return { ...state, ...payload, rebound: true }
    case CREATE_TASK_STATUS_SUCCESS:
      toast.success('Create status successfully')
      return { ...state, ...payload, rebound: true }
    case UPDATE_TASK_SUCCESS:
      toast.success('Update task successfully')
      return { ...state, ...payload, rebound: true }
    case DELETE_TASK_STATUS_SUCCESS:
      toast.success('Delete task successfully')
      return { ...state, ...payload, rebound: true }
    default:
      return state
  }
}
