import { ILoadingType } from 'store/actions/loading/loadingAction.d'
import { IAppCreator } from '../app/appReducer.d'
import { ILoadingState } from './loadingReducer.d'
const initialState: ILoadingState = {
    totalRequests: 0,
}

export default (state = initialState, { type, payload }: IAppCreator) => {
    switch (type) {
        case ILoadingType.START_LOADING:
            return { ...state, totalRequests: state.totalRequests + 1 }
        case ILoadingType.FINISH_LOADING:
            return { ...state, totalRequests: state.totalRequests - 1 }
        default:
            return state
    }
}
