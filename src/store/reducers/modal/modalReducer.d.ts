export interface IModalState {
    isOpen: boolean,
    modalType: ModalType,
    modalProps: IModalProps
}
export type ModalType = "CONFIRM" | "MESSAGE" | "CUSTOM"
export interface IModalPropsValues {
    handleClose: () => void
    handleConfirm?: () => void
    title?: React.ReactNode | string
    content: React.ReactNode | string
}
export interface IModalCreator {
    type: string
    payload: any
}
