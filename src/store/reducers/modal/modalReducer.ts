import { IModalType } from 'store/actions/modal/modalAction.d'
import { IModalCreator, IModalState, ModalType } from './modalReducer.d'
const initialState: IModalState = {
    isOpen: false,
    modalProps: {},
    modalType: "CUSTOM"
}

export default (state = initialState, { type, payload }: IModalCreator) => {
    switch (type) {
        case IModalType.OPEN_MODAL:
            return { ...state, ...payload, isOpen: true, }
        case IModalType.HIDE_MODAL:
            return { ...state, isOpen: false }
        default:
            return state
    }
}
