import { createSelector } from "reselect";
import { IRootState } from "store/rootReducers.d";

export const isOpenSelector = createSelector(
    (state: IRootState) => state.modal,
    (modal) => modal.isOpen
)

export const modalPropsSelector = createSelector(
    (state: IRootState) => state.modal,
    (modal) => modal.modalProps
)

export const modalTypeSelector = createSelector(
    (state: IRootState) => state.modal,
    (modal) => modal.modalType
)