import { createSelector } from "reselect";
import { IRootState } from "store/rootReducers.d";

export const isLoadingSelector = createSelector(
    (state: IRootState) => state.loading,
    (loading) => {
        return loading.totalRequests > 0
    }
)
