import { createSelector } from 'reselect'
import { IRootState } from 'store/rootReducers.d'
export const taskSelector = createSelector(
  (state: IRootState) => state.task,
  (task) => task
)
