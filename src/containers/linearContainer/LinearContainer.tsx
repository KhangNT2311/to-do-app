import { LinearProgress } from '@material-ui/core'
import Modal from 'components/molecules/modal/Modal'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { isLoadingSelector } from 'store/selectors/loading/loadingSelector'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import './LinearContainer.scss'

const LinearContainer: React.FC = ({ children }) => {
  const isLoading = useSelector(isLoadingSelector)
  return (
    <div>
      <ToastContainer autoClose={3000} />
      {isLoading && (
        <div className="loading">
          <LinearProgress />
        </div>
      )}
      <Modal />
      {children}
    </div>
  )
}
export default LinearContainer
