const MODEL = {
  HOME: 'Home',
  TASK: 'Task',
  TASK_STATUS: 'TaskStatus',
}
module.exports = {
  MODEL,
}
