const httpStatus = require('http-status')
const TaskStatus = require('../models/taskStatus.model')
const Task = require('../models/task.model')

exports.load = async (req, res, next, id) => {

  try {
    const taskStatus = await TaskStatus.get(id)
    req.locals = { taskStatus }
    return next()
  } catch (error) {
    return next(error)
  }
}

/**
 * Returns taskStatuses
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const taskStatuses = await TaskStatus.list(req.query)
    res.json(taskStatuses)
  } catch (error) {
    next(error)
  }
}

exports.get = (req, res) => res.json(req.locals.taskStatus)

exports.create = async (req, res, next) => {
  try {
    const taskStatus = new TaskStatus(req.body)
    const savedTaskStatus = await taskStatus.save()
    res.status(httpStatus.CREATED)
    res.json(savedTaskStatus)
  } catch (error) {
    next(error)
  }
}

exports.replace = async (req, res, next) => {
  const taskStatus = Object.assign(req.locals.taskStatus, req.body)

  taskStatus
    .save()
    .then((savedTaskStatus) => res.json(savedTaskStatus))
    .catch((e) => next(e))
}

exports.delete = async (req, res, next) => {
  try {
    const { taskStatus } = req.locals
    const deletedTaskStatus = await TaskStatus.deleteOne(taskStatus)
    res.json(deletedTaskStatus)
  } catch (error) {
    next(error)
  }
}

exports.changeStatus = async (req, res, next) => {
  try {
    const source = req.body.source
    const destination = req.body.destination
    const itemSource = await TaskStatus.findById(source.statusId)
    let task
    if (source.statusId === destination.statusId) {
      const itemRemoved = itemSource?.tasks.splice(source.index, 1)
      task = await Task.findById(itemRemoved?.[0]._id)
      task.taskStatus = itemSource._id

      if (itemRemoved) {
        itemSource?.tasks.splice(destination.index, 0, itemRemoved?.[0])
      }
      itemSource.save()
    } else {
      const itemDestination = await TaskStatus.findById(destination.statusId)
      const itemRemoved = itemSource?.tasks.splice(source.index, 1)
      task = await Task.findById(itemRemoved?.[0]._id)
      task.taskStatus = itemDestination._id
      if (itemRemoved) {
        itemDestination?.tasks.splice(destination.index, 0, itemRemoved?.[0])
      }
      itemSource.save()
      itemDestination.save()
    }
    task.save()
    res.status(200)
    res.json({ data: 'Update successfully' })
  } catch (error) {
    next(error)
  }
}

exports.reorder = async (req, res, next) => {
  try {
    const updatedTaskStatuses = req.body.updatedTaskStatuses
    updatedTaskStatuses.forEach(async (item, idx) => {
      const taskStatus = await TaskStatus.findById(item._id)
      taskStatus.sortPriority = idx + 1
      taskStatus.save()
    })

    res.status(200)
    res.json({ data: 'Update successfully' })
  } catch (error) {
    next(error)
  }
}
