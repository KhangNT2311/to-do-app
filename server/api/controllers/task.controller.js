const httpStatus = require('http-status')
const Task = require('../models/task.model')
const TaskStatus = require('../models/taskStatus.model')
const _ = require('lodash')
exports.load = async (req, res, next, id) => {
  try {
    const task = await Task.get(id)
    req.locals = { task }
    return next()
  } catch (error) {
    return next(error)
  }
}

/**
 * Returns tasks
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const tasks = await Task.list(req.query)
    res.json(tasks)
  } catch (error) {
    next(error)
  }
}

exports.get = (req, res) => res.json(req.locals.task)

exports.create = async (req, res, next) => {
  try {
    const task = new Task(req.body)
    const savedTask = await task.save()
    const taskStatus = await TaskStatus.findById(task.taskStatus)
    taskStatus.tasks.push(savedTask)
    taskStatus.save()
    res.status(httpStatus.CREATED)
    res.json(savedTask)
  } catch (error) {
    next(error)
  }
}

exports.replace = async (req, res, next) => {
  if (req.locals.task.taskStatus !== req.body.taskStatus) {
    const itemSource = await TaskStatus.findById(req.locals.task.taskStatus)
    const itemDestination = await TaskStatus.findById(req.body.taskStatus)
    const itemIdx = itemSource?.tasks.findIndex(
      (task) => task.toString() === req.locals.task._id.toString()
    )
    const itemRemoved = itemSource?.tasks.splice(itemIdx, 1)
    if (itemRemoved) {
      itemDestination?.tasks.splice(0, 0, itemRemoved?.[0])
    }
    itemSource.save()
    itemDestination.save()
  }
  const task = Object.assign(req.locals.task, req.body)
  task
    .save()
    .then((savedTask) => res.json(savedTask))
    .catch((e) => next(e))
}

exports.delete = async (req, res, next) => {
  try {
    const { task } = req.locals
    const deletedTask = await Task.deleteOne(task)
    res.json(deletedTask)
  } catch (error) {
    next(error)
  }
}
