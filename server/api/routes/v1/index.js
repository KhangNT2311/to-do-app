const express = require('express')
const taskRoutes = require('./task.route')
const taskStatusRoutes = require('./taskStatus.route')

const router = express.Router()

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'))


router.use('/tasks', taskRoutes)
router.use('/taskStatuses', taskStatusRoutes)

module.exports = router
