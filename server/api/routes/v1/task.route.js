const express = require('express')
const controller = require('../../controllers/task.controller')

const router = express.Router()

router.param('taskId', controller.load)

router.route('/').get(controller.list).post(controller.create)

router
  .route('/:taskId')
  .get(controller.get)
  .delete(controller.delete)
  .put(controller.replace)

module.exports = router
