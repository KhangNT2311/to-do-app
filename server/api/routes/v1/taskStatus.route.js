const express = require('express')
const controller = require('../../controllers/taskStatus.controller')

const router = express.Router()

router.route('/changeStatus').put(controller.changeStatus)
router.route('/reorder').put(controller.reorder)

router.param('taskStatusId', controller.load)

router.route('/').get(controller.list).post(controller.create)

router
  .route('/:taskStatusId')
  .get(controller.get)
  .delete(controller.delete)
  .put(controller.replace)


module.exports = router
