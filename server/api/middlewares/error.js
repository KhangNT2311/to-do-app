const httpStatus = require('http-status')
const APIError = require('../errors/api-error')

const handler = (err, req, res, next) => {
  const response = {
    code: err.status || 500,
    message: err.message || httpStatus[err.status || 500],
    errors: err.errors,
    stack: err.stack,
  }

  res.status(err.status || 500)
  res.json(response)
}
exports.handler = handler

exports.converter = (err, req, res, next) => {
  let convertedError = err

  if (!(err instanceof APIError)) {
    convertedError = new APIError({
      message: err.message,
      status: err.status,
      stack: err.stack,
    })
  }

  return handler(convertedError, req, res)
}

exports.notFound = (req, res, next) => {

  const err = new APIError({
    message: 'Not found',
    status: httpStatus.NOT_FOUND,
  })
  return handler(err, req, res)
}
