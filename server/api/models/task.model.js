const mongoose = require('mongoose')
const { omitBy, isNil } = require('lodash')
const { MODEL } = require('../../constants/enums')
const APIError = require('../errors/api-error')
const httpStatus = require('http-status')

/**
 * Task Schema
 * @private
 */
const taskSchema = new mongoose.Schema(
  {
    taskName: {
      type: String,
      required: true,
      index: true,
    },
    taskDescription: {
      type: String,
      required: true,
    },
    taskImages: [String],
    sortPriority: {
      type: Number,
    },
    taskStatus: {
      type: mongoose.Schema.Types.ObjectId,
      ref: MODEL.TASK_STATUS,
    },
  },
  {
    timestamps: true,
  }
)
/**
 * Statics
 */
taskSchema.statics = {
  async get(id) {
    let task

    if (mongoose.Types.ObjectId.isValid(id)) {
      task = await this.findById(id).exec()
    }
    if (task) {
      return task
    }

    throw new APIError({
      message: 'Task does not exist',
      status: httpStatus.NOT_FOUND,
    })
  },
  /**
   * List tasks in descending order of 'createdAt' timestamp.
   *
   * @param {number} skip - Number of tasks to be skipped.
   * @param {number} limit - Limit number of tasks to be returned.
   * @returns {Promise<Task[]>}
   */ list({ page = 1, perPage = 100, homeName, ...ops }) {
    const options = omitBy({ homeName, ...ops }, isNil)

    return this.find(options)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .populate('taskStatus')
      .lean()
      .exec()
  },
}

/**
 * @typedef Task
 */
module.exports = mongoose.model(MODEL.TASK, taskSchema)
