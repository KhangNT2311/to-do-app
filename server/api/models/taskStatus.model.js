const mongoose = require('mongoose')
const { omitBy, isNil } = require('lodash')
const { MODEL } = require('../../constants/enums')
const APIError = require('../errors/api-error')
const httpStatus = require('http-status')
/**
 * Task Status Schema
 * @private
 */
const taskStatusSchema = new mongoose.Schema({
  taskStatusName: {
    type: String,
    required: true,
    index: true,
  },
  sortPriority: {
    type: Number,
  },
  tasks: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: MODEL.TASK,
    },
  ],
})
/**
 * Statics
 */
taskStatusSchema.statics = {
  async get(id) {
    let taskStatus

    if (mongoose.Types.ObjectId.isValid(id)) {
      taskStatus = await this.findById(id).exec()
    }
    if (taskStatus) {
      return taskStatus
    }

    throw new APIError({
      message: 'Task status does not exist',
      status: httpStatus.NOT_FOUND,
    })
  },
  /**
   * List users in descending order of 'createdAt' timestamp.
   *
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<Home[]>}
   */ list({ page = 1, perPage = 30, ...ops }) {
    const options = omitBy({ ...ops }, isNil)

    return this.find(options)
      .sort('sortPriority')
      .skip(perPage * (page - 1))
      .limit(perPage)
      .populate('tasks')
      .lean()
  },
}

/**
 * @typedef TaskStatus
 */
module.exports = mongoose.model(MODEL.TASK_STATUS, taskStatusSchema)
